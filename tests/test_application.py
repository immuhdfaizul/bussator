#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import pytest
import requests_mock


@pytest.fixture
def config():
    return ('[General]\n'
            'enabled_plugins = dummy')


@pytest.fixture
def application(tmpdir, config):
    f = tmpdir.join('bussator.ini')
    f.write(config)

    from bussator import make_app
    return make_app(os.path.join(f.dirname, f.basename))


@pytest.fixture
def request_body():
    return b''

@pytest.fixture
def environment(request_body):
    from wsgiref.util import setup_testing_defaults
    env = {}
    setup_testing_defaults(env)

    from io import BytesIO
    env['wsgi.input'] = BytesIO(request_body)
    return env


class StartResponse:
    def __init__(self):
        self.status = None
        self.headers = []

    def __call__(self, status, headers):
        self.status = status
        self.headers = headers


@pytest.fixture
def start_response():
    return StartResponse()


def test_empty(application, environment, start_response):
    body = application(environment, start_response)
    next(body)
    assert start_response.status == '400 Bad Request'
    assert start_response.headers == []
    

@pytest.mark.parametrize('request_body', [
    pytest.param(b'target=http://example.org&source=htt',
                 id='truncated URL'),
    pytest.param(b'target=http://example.org&source=ftp://www.mardy.it',
                 id='ftp link'),
    ])
def test_invalid_source(application, environment, start_response):
    body = application(environment, start_response)
    next(body)
    assert start_response.status == '400 Bad Request'
    assert start_response.headers == []


@pytest.mark.parametrize('request_body,source_html', [
    pytest.param(b'target=http://example.org&source=http://www.mardy.it',
                 None,
                 id='empty page'),
    pytest.param(b'target=http://example.org&source=http://www.mardy.it',
                 'no-links.html',
                 id='no links'),
    pytest.param(b'target=http://example.org&source=http://www.mardy.it',
                 'other-link.html',
                 id='other link'),
    ])
def test_source_contents(application, environment, start_response,
                         source_html):
    if source_html:
        curr_dir = os.path.dirname(os.path.abspath(__file__))
        html_file = open(os.path.join(curr_dir, 'data', source_html), 'rb')
    else:
        html_file = None

    with requests_mock.Mocker() as mock:
        mock.get('http://www.mardy.it', body=html_file)
        body_parts = application(environment, start_response)
        body = next(body_parts)
        assert start_response.status == '202 Accepted'
        assert body == (b'Processing webmention request:\n'
                        b'  target: http://example.org\n'
                        b'  source: http://www.mardy.it\n')
        try:
            body = next(body_parts)
        except StopIteration:
            pass


@pytest.mark.parametrize('request_body,source_html,expected_webmention', [
    pytest.param(b'target=http://example.org/my-post.html&source=http://www.mardy.it',
                 'other-link.html',
                 {
                     'source': 'http://www.mardy.it',
                     'target': 'http://example.org/my-post.html',
                     'post_data': {
                         'author_email': None,
                         'author_name': None,
                         'author_photo': None,
                         'author_url': None,
                         'has_mf': True,
                         'is_like': False,
                         'post_content_html': '',
                         'post_content_text': '',
                         'post_date': None,
                         'post_title': '',
                         'post_url': None},
                 },
                 id='other link'),
    ])
def test_webmention(application, environment, start_response,
                    source_html, expected_webmention):
    curr_dir = os.path.dirname(os.path.abspath(__file__))
    html_file = open(os.path.join(curr_dir, 'data', source_html), 'rb')

    with requests_mock.Mocker() as mock:
        mock.get('http://www.mardy.it', body=html_file)
        body_parts = application(environment, start_response)
        body = next(body_parts)
        assert start_response.status == '202 Accepted'
        try:
            body = next(body_parts)
        except StopIteration:
            pass

    assert len(application.active_plugins) == 1
    plugin = application.active_plugins[0]
    from bussator.plugins.dummy import DummyPlugin
    assert isinstance(plugin, DummyPlugin)

    assert len(plugin.process_webmention_calls) == 1

    call_data = plugin.process_webmention_calls[0]
    assert call_data == expected_webmention
