# Shamelessly stolen from the Isso Dockerfile, minus the bits that are
# different.
FROM python:3.8-buster AS venvBuilder
WORKDIR /src/
COPY . .
RUN python3 -m venv /bussator \
        && . /bussator/bin/activate \
        && pip3 install --no-cache-dir --upgrade pip \
        && pip3 install --no-cache-dir gunicorn \
        && python setup.py install \
        && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

FROM python:3.8-slim-buster
WORKDIR /bussator/
COPY --from=venvBuilder /bussator .

# Ensure the volume is mounted and contains a config.ini.
VOLUME /config
EXPOSE 8080
CMD ["/bussator/bin/gunicorn", "-b", "0.0.0.0:8080", "-w", "4", "--preload", "bussator.run", "--worker-tmp-dir", "/dev/shm"]

